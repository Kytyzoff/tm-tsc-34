package ru.tsc.borisyuk.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.tsc.borisyuk.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListResponse extends AbstractResponse {

    private List<Project> projects;

    public ProjectListResponse(final List<Project> projects) {
        this.projects = projects;
    }

}
