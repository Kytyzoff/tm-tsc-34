package ru.tsc.borisyuk.tm.dto.response;

import lombok.NoArgsConstructor;
import ru.tsc.borisyuk.tm.model.User;

@NoArgsConstructor
public class UserRegisterResponse extends AbstractUserResponse {

    public UserRegisterResponse(final User user) {
        super(user);
    }

}