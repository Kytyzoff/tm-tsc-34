package ru.tsc.borisyuk.tm.api.endpoint;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IProjectEndpoint extends IEndpoint {

    String NAME = "ProjectEndpoint";

    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NonNull final String host, @NonNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, NAMESPACE, PART, IProjectEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NonNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, NAMESPACE, PART, IProjectEndpoint.class);
    }

    @NonNull
    @WebMethod
    ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectChangeStatusByIdRequest request
    );

    @NonNull
    @WebMethod
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectChangeStatusByIndexRequest request
    );

    @NonNull
    @WebMethod
    ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectRemoveByIdRequest request
    );

    @NonNull
    @WebMethod
    ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectRemoveByIndexRequest request
    );

    @NonNull
    @WebMethod
    ProjectGetByIdResponse getProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectGetByIdRequest request
    );

    @NonNull
    @WebMethod
    ProjectGetByIndexResponse getProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectGetByIndexRequest request
    );

    @NonNull
    @WebMethod
    ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectUpdateByIdRequest request
    );

    @NonNull
    @WebMethod
    ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectUpdateByIndexRequest request
    );

    @NonNull
    @WebMethod
    ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectCreateRequest request
    );

    @NonNull
    @WebMethod
    ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectListRequest request
    );

    @NonNull
    @WebMethod
    ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NonNull final ProjectClearRequest request
    );

}

