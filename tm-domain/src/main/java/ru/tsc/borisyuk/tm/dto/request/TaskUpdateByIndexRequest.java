package ru.tsc.borisyuk.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TaskUpdateByIndexRequest extends AbstractUserRequest {

    private Integer index;

    private String name;

    private String description;

}
