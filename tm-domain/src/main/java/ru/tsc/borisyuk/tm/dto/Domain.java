package ru.tsc.borisyuk.tm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.model.Task;
import ru.tsc.borisyuk.tm.model.User;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement
@XmlType(name = "domain")
@XmlAccessorType(XmlAccessType.FIELD)
@JacksonXmlRootElement(localName = "domain")
public class Domain implements Serializable {

    @NonNull
    private static final long serialVersionUID = 1;

    @NonNull
    private String id = UUID.randomUUID().toString();

    @NonNull
    private Date created = new Date();

    @NonNull
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    @JsonProperty("user")
    @JacksonXmlElementWrapper(localName = "users")
    private List<User> users = new ArrayList<>();

    @NonNull
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    @JsonProperty("task")
    @JacksonXmlElementWrapper(localName = "tasks")
    private List<Task> tasks = new ArrayList<>();

    @NonNull
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    @JsonProperty("project")
    @JacksonXmlElementWrapper(localName = "projects")
    private List<Project> projects = new ArrayList<>();

}
