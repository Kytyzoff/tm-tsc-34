package ru.tsc.borisyuk.tm.api.repository;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NonNull
    List<M> findAll();

    @NonNull
    List<M> findAll(Comparator comparator);

    M add(M item);

    Collection<M> addAll(Collection<M> collection);

    M remove(M item);

    void removeAll(Collection<M> collection);

    Collection<M> set(Collection<M> collection);

    void clear();

    M findOneByIndex(Integer index);

    M findOneById(String id);

    int size();

}
