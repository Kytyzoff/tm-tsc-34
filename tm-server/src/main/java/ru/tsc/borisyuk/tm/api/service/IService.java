package ru.tsc.borisyuk.tm.api.service;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.repository.IRepository;
import ru.tsc.borisyuk.tm.enumerated.Sort;
import ru.tsc.borisyuk.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    M removeById(String id);

    M removeByIndex(Integer index);

    @NonNull
    List<M> findAll(Sort sort);

}
