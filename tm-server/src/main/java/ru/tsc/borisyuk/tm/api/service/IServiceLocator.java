package ru.tsc.borisyuk.tm.api.service;

import lombok.NonNull;

public interface IServiceLocator {

    @NonNull
    ITaskService getTaskService();

    @NonNull
    IProjectService getProjectService();

    @NonNull
    IProjectTaskService getProjectTaskService();

    @NonNull
    ILoggerService getLoggerService();

    @NonNull
    IUserService getUserService();

    @NonNull
    IAuthService getAuthService();

    @NonNull
    IPropertyService getPropertyService();

    @NonNull
    IDomainService getDomainService();

}
