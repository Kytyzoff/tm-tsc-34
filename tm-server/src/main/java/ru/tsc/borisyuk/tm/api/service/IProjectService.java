package ru.tsc.borisyuk.tm.api.service;

import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnedService<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

    Project create(String userId, String name, String description, Date dateBegin, Date dateEnd);

    Project updateByIndex(String userId, Integer index, String name, String description);

    Project updateById(String userId, String id, String name, String description);

    Project changeStatusByIndex(String userId, Integer index, Status status);

    Project changeStatusById(String userId, String id, Status status);

}
