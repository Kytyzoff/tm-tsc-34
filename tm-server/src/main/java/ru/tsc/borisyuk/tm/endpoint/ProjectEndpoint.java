package ru.tsc.borisyuk.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.borisyuk.tm.api.service.IServiceLocator;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;
import ru.tsc.borisyuk.tm.enumerated.Sort;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.model.Session;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.tsc.borisyuk.tm.api.endpoint.IProjectEndpoint")
@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NonNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NonNull
    @Override
    public ProjectChangeStatusByIdResponse changeProjectStatusById(@NonNull final ProjectChangeStatusByIdRequest request) {
        check(request);
        final String id = request.getId();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Status status = request.getStatus();
        final Project project = getProjectService().changeStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NonNull
    @Override
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NonNull final ProjectChangeStatusByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Status status = request.getStatus();
        final Project project = getProjectService().changeStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NonNull
    @Override
    public ProjectRemoveByIdResponse removeProjectById(@NonNull final ProjectRemoveByIdRequest request) {
        check(request);
        final String id = request.getId();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Project project = getProjectService().removeById(userId, id);
        return new ProjectRemoveByIdResponse(project);
    }

    @NonNull
    @Override
    public ProjectRemoveByIndexResponse removeProjectByIndex(@NonNull final ProjectRemoveByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Project project = getProjectService().removeByIndex(userId, index);
        return new ProjectRemoveByIndexResponse(project);
    }

    @NonNull
    @Override
    public ProjectGetByIdResponse getProjectById(@NonNull final ProjectGetByIdRequest request) {
        check(request);
        final String id = request.getId();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Project project = getProjectService().findOneById(userId, id);
        return new ProjectGetByIdResponse(project);
    }

    @NonNull
    @Override
    public ProjectGetByIndexResponse getProjectByIndex(@NonNull final ProjectGetByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Project project = getProjectService().findOneByIndex(userId, index);
        return new ProjectGetByIndexResponse(project);
    }

    @NonNull
    @Override
    public ProjectUpdateByIdResponse updateProjectById(@NonNull final ProjectUpdateByIdRequest request) {
        check(request);
        final String id = request.getId();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final String name = request.getName();
        final String description = request.getDescription();
        final Project project = getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @NonNull
    @Override
    public ProjectUpdateByIndexResponse updateProjectByIndex(@NonNull final ProjectUpdateByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final String name = request.getName();
        final String description = request.getDescription();
        final Project project = getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

    @NonNull
    @Override
    public ProjectCreateResponse createProject(@NonNull final ProjectCreateRequest request) {
        check(request);
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final String name = request.getName();
        final String description = request.getDescription();
        final Project project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NonNull
    @Override
    public ProjectListResponse listProject(@NonNull final ProjectListRequest request) {
        check(request);
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        final Sort sort = request.getSort();
        final List<Project> projects = getProjectService().findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @NonNull
    @Override
    public ProjectClearResponse clearProject(@NonNull final ProjectClearRequest request) {
        check(request);
        @NonNull final Session session = check(request);
        final String userId = session.getUserId();
        getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

}
