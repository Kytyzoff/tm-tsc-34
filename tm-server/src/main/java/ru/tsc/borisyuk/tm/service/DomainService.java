package ru.tsc.borisyuk.tm.service;

import ru.tsc.borisyuk.tm.api.service.IDomainService;

public class DomainService implements IDomainService {

    @Override
    public void loadDataBackup() {
    }

    @Override
    public void saveDataBackup() {
    }

    @Override
    public void loadDataBase64() {
    }

    @Override
    public void saveDataBase64() {
    }

    @Override
    public void loadDataBinary() {
    }

    @Override
    public void saveDataBinary() {
    }

    @Override
    public void loadDataJsonFaster() {
    }

    @Override
    public void saveDataJsonFaster() {
    }

    @Override
    public void loadDataJsonJaxb() {
    }

    @Override
    public void saveDataJsonJaxb() {
    }

    @Override
    public void loadDataXmlFaster() {
    }

    @Override
    public void saveDataXmlFaster() {
    }

    @Override
    public void loadDataXmlJaxb() {
    }

    @Override
    public void saveDataXmlJaxb() {
    }

    @Override
    public void loadDataYamlFaster() {
    }

    @Override
    public void saveDataYamlFaster() {
    }

}
