package ru.tsc.borisyuk.tm.util;

import lombok.NonNull;

import java.util.Date;
import java.util.Scanner;

public interface TerminalUtil {

    @NonNull
    Scanner SCANNER = new Scanner(System.in);

    @NonNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NonNull
    static Integer nextNumber() {
        return Integer.parseInt(SCANNER.nextLine());
    }

    static Date nextDate() {
        return DateUtil.toDate(SCANNER.nextLine());
    }

}
