package ru.tsc.borisyuk.tm.repository;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.repository.IRepository;
import ru.tsc.borisyuk.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NonNull
    protected final List<M> items = new CopyOnWriteArrayList<>();

    @NonNull
    @Override
    public List<M> findAll() {
        return items;
    }

    @NonNull
    @Override
    public List<M> findAll(@NonNull final Comparator comparator) {
        @NonNull final List<M> result = new CopyOnWriteArrayList<>(items);
        result.sort(comparator);
        return result;
    }

    @NonNull
    @Override
    public M add(@NonNull final M item) {
        items.add(item);
        return item;
    }

    @NonNull
    @Override
    public Collection<M> addAll(@NonNull final Collection<M> collection) {
        items.addAll(collection);
        return items;
    }

    @NonNull
    @Override
    public M remove(@NonNull final M item) {
        items.remove(item);
        return item;
    }

    @Override
    public void removeAll(@NonNull final Collection<M> collection) {
        items.removeAll(collection);
    }

    @NonNull
    @Override
    public Collection<M> set(@NonNull final Collection<M> collection) {
        clear();
        return addAll(collection);
    }

    @Override
    public void clear() {
        items.clear();
    }

    @NonNull
    @Override
    public M findOneByIndex(@NonNull final Integer index) {
        return items.get(index);
    }

    @Override
    public M findOneById(@NonNull final String id) {
        return items.stream().filter(project -> id.equals(project.getId()))
                .findAny().orElse(null);
    }

    @Override
    public int size() {
        return items.size();
    }

}
