package ru.tsc.borisyuk.tm.api.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractSoapClient {

    private String host = "localhost";

    private String port = "8080";

}
