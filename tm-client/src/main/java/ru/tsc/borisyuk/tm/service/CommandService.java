package ru.tsc.borisyuk.tm.service;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import ru.tsc.borisyuk.tm.api.repository.ICommandRepository;
import ru.tsc.borisyuk.tm.api.service.ICommandService;
import ru.tsc.borisyuk.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandService implements ICommandService {

    @NonNull
    private final ICommandRepository commandRepository;

    public CommandService(@NonNull ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NonNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @NonNull
    @Override
    public Collection<AbstractCommand> getArgumentCommands() {
        return commandRepository.getArgumentCommands();
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        if (StringUtils.isEmpty(name)) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argument) {
        if (StringUtils.isEmpty(argument)) return null;
        return commandRepository.getCommandByArgument(argument);
    }

}
